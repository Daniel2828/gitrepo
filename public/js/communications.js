/*
<li class="list-group-item" style="color:black">Dapibus ac facilisis in</li>
<li class="list-group-item list-group-item-primary">A simple primary list group item</li>
<li class="list-group-item list-group-item-secondary">A simple secondary list group item</li>
<li class="list-group-item list-group-item-success">A simple success list group item</li>
<li class="list-group-item list-group-item-danger">A simple danger list group item</li>
<li class="list-group-item list-group-item-warning">A simple warning list group item</li>
<li class="list-group-item list-group-item-info">A simple info list group item</li>
<li class="list-group-item list-group-item-light">A simple light list group item</li>
<li class="list-group-item list-group-item-dark">A simple dark list group item</li>
*/
// Declares


// ########################
// #######Ajax calls#######

// Button of FTP page
$('#btnFTPprocess').click(function(){

    $.ajax({
        crossDomain: true,
        dataType: 'json',
        type: 'post',
        contentType: "application/json",
        data: JSON.stringify( {
            "title" : "Titulo1",
            "content":  $("#rutaPack").text() + $("#urlServer").val() ,
                ///user1/R8CVS2/home/PROGS/CS/ESSPE.src",
            "server": $("#inputGroupSelect01").val()
        }),
        url: "http://172.31.49.177:8080/api/notes",
        success: function (response) {
            // $('.greeting-id').append(response.title);
            console.log(response);
            var textToAppend;
            $('#listErrors').removeAttr('hidden');
            for (var i = 0; i < response.length; i++) {
                textToAppend ='<li class="list-group-item list-group-item-';
                 switch (response[i].type) {
                    case "0": textToAppend +='danger">'
                            break;
                    case "1": textToAppend +='success">'
                            break;
                    case "2": textToAppend +='warning">'
                            break;
                }
                textToAppend += response[i].description;
                textToAppend += '</li>';
                $('#listErrors').append(textToAppend);
               // $<li class="list-group-item list-group-item-dark">A simple dark list group item</li>
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
});
// Button of Login check page
$('#butposition').click(function(){
    $.ajax({
        type: 'POST',
        url: "http://172.31.49.177:8080/login",
        crossDomain: true,
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            'id': $("#inputUser").val(),
            'password': $("#inputPassword").val()

        }),
        success: function(response){
            if(response===false){
                alert("User doesn't exists or password isn't correct");
            }else{
                window.location.href = "home.html";
            }
        }
    });
});
// Button of Register page
$('#butRegisterPg').click(function(){
    if($("#registerPassword").val()===$("#registerRepeatPassword").val()){
        $.ajax({
            type: 'post',
            url: "http://172.31.49.177:8080/user",
            crossDomain: true,
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify({
                'id': $("#registerUser").val(),
                'password': $("#registerPassword").val()

            }),
            success: function(response){
                if(response===false){
                    alert("User doesn't exists or password isn't correct");
                }else{
                    window.location.href = "index.html";
                }
            },
            error:function (response) {
                    alert(response);
            }
        });
    }else {
        alert("The passwords are different");
    }

});
