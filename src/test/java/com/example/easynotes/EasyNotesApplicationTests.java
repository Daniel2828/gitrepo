package com.example.easynotes;

import com.example.easynotes.analyzer.AnalyzerConector;
import com.example.easynotes.model.Note;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Vector;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EasyNotesApplicationTests {

	@Test
	public void contextLoads() {
		Note note = new Note();
		AnalyzerConector aC  = new AnalyzerConector();
		Vector<String> lines = new Vector<String>();

		note.setContent("/user1/R8CVS2/home/PROGS/CS/ESSPE.src");
		note.setServer("172.31.49.13");

		aC.getFile(note, lines);

	}

}
