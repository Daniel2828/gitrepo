package com.example.easynotes.analyzer;

import com.example.easynotes.model.Note;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

public class AnalyzerConector {

    public boolean getFile(Note note, Vector<String> linesFile) {
        int port = 21;
        String user = "kccmrl";
        String pass = "kccmrl123";
        String remoteFile1 = note.getContent();
        String fileOutName = "C:\\Users\\roncerm\\Downloads\\" + System.currentTimeMillis() + ".src";
        File downloadFile1 = new File(fileOutName);
        boolean success = false;

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(note.getServer(), port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
            success = ftpClient.retrieveFile(remoteFile1, outputStream1);
            outputStream1.close();

            if (success) {
                System.out.println("File #1 has been downloaded successfully.");

                Files.lines(Paths.get(fileOutName)).forEach(s -> linesFile.addElement(s.toString()));
                if (linesFile.size() > 0 )
                    success = true;
                else
                    success = false;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return success;
    }
}
