package com.example.easynotes.analyzer;

import com.example.easynotes.model.CodeResult;
import com.example.easynotes.repository.CodeResultRepository;

import java.util.Vector;


public class Analyzer {

    CodeResultRepository repository;

    public Analyzer(CodeResultRepository repository) {
        this.repository = repository;
    }

    public void checkFile(String idCode, Vector<String> lines) {
        Integer iLine = 0;

        iLine = checkHeader(idCode, lines, iLine);

        while (iLine < lines.size()) {
            if (lines.elementAt(iLine).contains("REM ")) {
                iLine++;
            } else if (lines.elementAt(iLine).contains("DEFSUB '")) {
                iLine = checkSubroutine(idCode, lines, iLine);
            } else {
                iLine++;
            }
        }
    }

    /*
      Checks:     number line
                  $Id:
                  Comments in the header
    */
    public Integer checkHeader(String idCode, Vector<String> lines, Integer iLine) {

        CodeResult  codeResult;
        boolean bBreak = false;
        boolean bIdFound = false;

        if (lines.elementAt(1).startsWith("00010"))
            addCodeResult(idCode, "General", iLine, "2", "Line number: " + iLine + ". Incorrect start line number");
        else
            addCodeResult(idCode, "General", iLine, "1", "Line number: " + iLine + ". Correct start line number.");

        while (!bBreak && iLine < lines.size()) {
            iLine++;

            if (lines.elementAt(iLine).contains("$Id:")) {
                bIdFound = true;
                iLine++;
            }

            if (!lines.elementAt(iLine).contains("REM")) {
                iLine--;
                bBreak = true;
            }
        }

        if (!bIdFound)
            addCodeResult(idCode, "General", iLine, "0", "Line number: " + iLine + ". There are not $Id to cvs");
        else
            addCodeResult(idCode, "General", iLine, "1", "Line number: " + iLine + ". There are $Id to cvs");

        if (iLine <= 2)
            addCodeResult(idCode, "General", iLine, "0", "Line number: " + iLine + ". There are not comments on the top of the file");
        else
            addCodeResult(idCode, "General", iLine, "1", "Line number: " + iLine + ". There are comments on the top of the file");

        return iLine;
    }

    /*
        Check:  have comments
                Local dim only at the begining
                return only at the end
     */
    public Integer checkSubroutine(String idCode, Vector<String> lines, Integer iLine) {
        boolean bHasComment = false;
        boolean bLocalDim = true;
        boolean bFinish = false;
        boolean bOpenOneFile = false;


        CodeResult  codeResult;
        String subRutineName = new String();

        // STOP, PANIC GOTO (0)
        // open_one_file (2)

        subRutineName = lines.elementAt(iLine);
        iLine++;
        while (iLine < lines.size() && lines.elementAt(iLine).contains("REM ")) {
            bHasComment = true;
            iLine++;
        }

        while (iLine < lines.size() && lines.elementAt(iLine).contains("LOCAL DIM ")) {
            iLine++;
            // pasar el parse de local dim
        }

        while (!bFinish && iLine < lines.size()) {
            iLine++;
            if (lines.elementAt(iLine).contains("REM ")) {
                continue;
            }

            if (lines.elementAt(iLine).contains("END SUB") || lines.elementAt(iLine).contains("RETURN ")) {
                bFinish = true;
                iLine++;
            }

            if (lines.elementAt(iLine).contains("LOCAL DIM ")) {
                bLocalDim = false;
            }

            if (lines.elementAt(iLine).contains("r7_open_one_file")) {
                bOpenOneFile = true;
            }

            if (lines.elementAt(iLine).contains(" PANIC"))
                addCodeResult(idCode, "Subroutine", iLine, "0", "Line number: " + iLine + ". Subroutine " + subRutineName + " contains PANIC sentence");

            if (lines.elementAt(iLine).contains(" GOTO"))
                addCodeResult(idCode, "Subroutine", iLine, "0", "Line number: " + iLine + ". Subroutine " + subRutineName + " contains GOTO sentence");

            if (lines.elementAt(iLine).contains(" STOP"))
                 addCodeResult(idCode, "Subroutine", iLine, "0", "Line number: " + iLine + ". Subroutine " + subRutineName + " contains STOP sentence");
        }

        if (!bHasComment)
            addCodeResult(idCode, "Subroutine", iLine, "0",  "Line number: " + iLine + ". Subroutine " + subRutineName + " has not any comment");
        else
            addCodeResult(idCode, "Subroutine", iLine, "1", "Line number: " + iLine + ". Subroutine " + subRutineName + " has comments");

        if (!bLocalDim)
            addCodeResult(idCode, "Subroutine", iLine, "0", "Line number: " + iLine + ". Subroutine " + subRutineName + " has variable definition inside the body part" );
        else
            addCodeResult(idCode, "Subroutine", iLine, "1", "Line number: " + iLine + ". Subroutine " + subRutineName + ". All varible definition are on the top" );

        if (bOpenOneFile)
            addCodeResult(idCode, "Subroutine", iLine, "2", "Line number: " + iLine + ". Subroutine " + subRutineName + " has a call to r7_open_one_file");

        return iLine;
    }

    public void addCodeResult(String idCode, String source, int iLine, String sType, String sDescription) {
        CodeResult codeResult = new CodeResult();
        codeResult.setIdSource(idCode);
        codeResult.setScope(source);
        codeResult.setLineNumber(iLine);
        codeResult.setType(sType);
        codeResult.setDescription(sDescription);

        if (this.repository != null)
            this.repository.save(codeResult);
    }
    public Integer checkVariables(String idCode, Vector<String> lines, Integer iLine, String subRoutineName) {


        return iLine;
    }
}
