package com.example.easynotes.controller;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.User;
import com.example.easynotes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://172.31.49.189", maxAge = 3600)
@RestController
public class UserController {
    @Autowired
    UserRepository userRepository;

    @PostMapping("/login")
    public boolean loginUser(@Valid @RequestBody User user) {
        boolean bResult = false;
        int iLoop = 0;

        if (userRepository.existsById(user.getId())) {
            try {
                List<User> us = userRepository.findAll();
                while (!bResult && iLoop <= us.size()) {
                    if (us.get(iLoop).getPassword().equals(user.getPassword()))
                        bResult = true;
                    iLoop++;
                }

            } catch (Exception ex) {
                bResult = false;
            }
        }

        return  bResult;
    }

    @PostMapping("/user")
    public boolean addUser(@Valid @RequestBody User user) {
        boolean bResult = true;

        if (userRepository.existsById(user.getId()))
            bResult = false;
        else
            userRepository.save(user);

        return bResult;
    }
}
