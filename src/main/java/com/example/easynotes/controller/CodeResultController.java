package com.example.easynotes.controller;

import com.example.easynotes.model.CodeResult;
import com.example.easynotes.repository.CodeResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CodeResultController {

    @Autowired
    CodeResultRepository codeResultRepository;

    @GetMapping("/results")
    public List<CodeResult> getAllNotes() {
        return codeResultRepository.findAll();
    }
}
