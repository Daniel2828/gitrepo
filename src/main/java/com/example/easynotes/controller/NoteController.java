package com.example.easynotes.controller;

import com.example.easynotes.analyzer.Analyzer;
import com.example.easynotes.analyzer.AnalyzerConector;
import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.CodeResult;
import com.example.easynotes.model.Note;
import com.example.easynotes.model.User;
import com.example.easynotes.repository.CodeResultRepository;
import com.example.easynotes.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Vector;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@CrossOrigin(origins = "http://172.31.49.189", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;
    @Autowired
    CodeResultRepository codeResultRepository;

    /**
     *
     * @return
     */
    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    @PostMapping("/notes")
    public List<CodeResult> createNote(@Valid @RequestBody Note note) {
        Vector<String> results = new Vector<String>();
        AnalyzerConector aC = new AnalyzerConector();
        Analyzer analyzer = new Analyzer(codeResultRepository);

        codeResultRepository.deleteAll();

        if (aC.getFile(note, results)) {
            System.out.println("File converted properly");
            analyzer.checkFile("aaaa", results);
        }

        noteRepository.save(note);

        return codeResultRepository.findAll();
    }

    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
    }

    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable(value = "id") Long noteId,
                                           @Valid @RequestBody Note noteDetails) {

        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Note updatedNote = noteRepository.save(note);
        return updatedNote;
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}
