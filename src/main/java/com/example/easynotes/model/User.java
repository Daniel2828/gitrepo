package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(allowGetters = true)
public class User {
    //@Id
    ///@GeneratedValue(strategy = GenerationType.IDENTITY)
    //7private Long id;

    // @NotBlank
    // @Column(unique=true)
    @Id
    private String id;

    @NotBlank
    private String password;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*public Long getId() {
        return id;
    }
    */

    /*
    public void setId(Long id) {
        this.id = id;
    }
    */

    @Override
    public boolean equals(Object obj) {
        return ((User)obj).getId() == this.id && ((User)obj).getPassword().equals(this.password);
    }
}
