package com.example.easynotes.repository;

import com.example.easynotes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {


    // @Query("FROM user u where u.username = :username and u.password = :password")
    // User findAllByUsernamePassword(@Param("username") String username, @Param("password") String password);

}
