package com.example.easynotes.repository;

import com.example.easynotes.model.CodeResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeResultRepository extends JpaRepository<CodeResult, Long> {

    // @Query("SELECT u.name FROM user u where u.name= :name")
    // String findUserByName(@Param("name") String name);
}
